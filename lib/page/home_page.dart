import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
            "Home Page",
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Color(0xff6883da)),
      body: Column(
        children: [
          Text("Hello World"),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: TextFormField(
              decoration: InputDecoration(
                  hintText: "Masukkan Nama ",
                  border: OutlineInputBorder(
                      borderSide: BorderSide(width: 30, color: Colors.blue))),
            ),
          ),
          ElevatedButton(onPressed: () {}, child: Text("Simpan"),
            style: ElevatedButton.styleFrom(backgroundColor: Colors.black38),
          )
        ],
      ),
    );
  }
}
